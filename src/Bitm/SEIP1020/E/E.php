<?php
namespace App\Bitm\SEIP1020\E;

use App\Bitm\SEIP1020\Utility\Utility;
use App\Bitm\SEIP1020\Message\Message;

class E
{
    public $id = "";
    public $mail = "";
    public $conn;
    public $deleted_at;


    public function prepare($data = "")
    {
        if (array_key_exists("mail", $data)) {
            $this->mail= $data['mail'];
        }
        if (array_key_exists("id", $data)) {
            $this->id = $data['id'];
        }

        //echo  $this;

    }

    public function __construct()
    {
        $this->conn = mysqli_connect("localhost", "root", "", "atomicprojectb22") or die("Database connection failed");
    }

    public function store()
    {
        $query = "INSERT INTO `atomicprojectb22`.`mail` (`mail`) VALUES ('" . $this->mail . "')";
        //echo $query;

        $result = mysqli_query($this->conn, $query);
        if ($result) {
            Message::message("
                <div class=\"alert alert-success\">
                            <strong>Success!</strong> Data has been stored successfully.
                </div>");
            Utility::redirect("index.php");
        } else {
            echo "Error";
        }
    }

    public function index()
    {
        $_allmail = array();
        $query = "SELECT * FROM `mail` WHERE `deleted_at` IS NULL";
        $result = mysqli_query($this->conn, $query);
        while ($row = mysqli_fetch_object($result)) {
            $_allmail[] = $row;
        }

        return $_allmail;


    }

    public function view()
    {
        $query = "SELECT * FROM `mail` WHERE `id`=" .$this->id;
        $result = mysqli_query($this->conn, $query);
        $row = mysqli_fetch_object($result);
        return $row;
    }

    public function update()
    {
        $query = "UPDATE `atomicprojectb22`.`mail` SET `mail` = '" . $this->mail . "' WHERE `mail`.`id` = " . $this->id;
        //echo $query;

        $result = mysqli_query($this->conn, $query);
        if ($result) {
            Message::message("
<div class=\"alert alert-info\">
  <strong>Success!</strong> Data has been updated  successfully.
</div>");
            Utility::redirect("index.php");
        } else {
            echo "Error";
        }
    }

    public function delete()
    {
        $query = "DELETE FROM `atomicprojectb22`.`mail` WHERE `mail`.`id` = " . $this->id;
        $result = mysqli_query($this->conn, $query);
        if ($result) {
            Message::message("
<div class=\"alert alert-info\">
  <strong>Deleted!</strong> Data has been deleted successfully.
</div>");
            Utility::redirect("index.php");
        } else {
            Message::message("
<div class=\"alert alert-info\">
  <strong>Deleted!</strong> Data has not been deleted successfully.
</div>");
            Utility::redirect("index.php");
        }

    }

    public function trash()
    {
        $this->deleted_at = time();
        $query = "UPDATE `atomicprojectb22`.`mail` SET `deleted_at` =" . $this->deleted_at . " WHERE `mail`.`id` = " . $this->id;
        $result = mysqli_query($this->conn, $query);
        if ($result) {
            Message::message("
<div class=\"alert alert-info\">
  <strong>Deleted!</strong> Data has been trashed successfully.
</div>");
            Utility::redirect("index.php");
        } else {
            Message::message("
<div class=\"alert alert-info\">
  <strong>Deleted!</strong> Data has not been trashed successfully.
</div>");
            Utility::redirect("index.php");
        }

    }

    public function trashed()
    {
        $_allmail = array();
        $query = "SELECT * FROM `mail` WHERE `deleted_at` IS NOT NULL";
        $result = mysqli_query($this->conn, $query);
        while ($row = mysqli_fetch_object($result)) {
            $_allmail[] = $row;
        }

        return $_allmail;


    }

    public function recover()
    {

        $query = "UPDATE `atomicprojectb22`.`mail` SET `deleted_at` = NULL WHERE `mail`.`id` = " . $this->id;
        $result = mysqli_query($this->conn, $query);
        if ($result) {
            Message::message("
<div class=\"alert alert-info\">
  <strong>Deleted!</strong> Data has been recovered successfully.
</div>");
            Utility::redirect("index.php");
        } else {
            Message::message("
<div class=\"alert alert-info\">
  <strong>Deleted!</strong> Data has not been recovered successfully.
</div>");
            Utility::redirect("index.php");
        }

    }

    public function recoverSeleted($IDs = Array())
    {
        if ((is_array($IDs)) && (count($IDs > 0))) {
            $ids = implode(",", $IDs);
            $query = "UPDATE `atomicprojectb22`.`mail` SET `deleted_at` = NULL WHERE `mail`.`id` IN(" . $ids . ")";
            //echo $query;
            //die();
            $result = mysqli_query($this->conn, $query);
            if ($result) {
                Message::message("
<div class=\"alert alert-info\">
  <strong>Deleted!</strong> Selected Data has been recovered successfully.
</div>");
                Utility::redirect("index.php");
            } else {
                Message::message("
<div class=\"alert alert-info\">
  <strong>Deleted!</strong> Selected Data has not been recovered successfully.
</div>");
                Utility::redirect("index.php");
            }

        }

    }

    public function deleteMultiple($IDs = Array())
    {
        if ((is_array($IDs)) && (count($IDs > 0))) {
            $ids = implode(",", $IDs);
            $query = "DELETE FROM `atomicprojectb22`.`mail` WHERE `mail`.`id`  IN(" . $ids . ")";
            $result = mysqli_query($this->conn, $query);
            if ($result) {
                Message::message("
<div class=\"alert alert-info\">
  <strong>Deleted!</strong> Selected Data has been deleted successfully.
</div>");
                Utility::redirect("index.php");
            } else {
                Message::message("
<div class=\"alert alert-info\">
  <strong>Deleted!</strong> Selected Data has not been deleted successfully.
</div>");
                Utility::redirect("index.php");
            }

        }
    }




}