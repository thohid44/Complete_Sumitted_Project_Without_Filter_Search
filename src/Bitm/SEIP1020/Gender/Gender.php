<?php

namespace App\Bitm\SEIP1020\Gender;

use App\Bitm\SEIP1020\Utility\Utility;
use App\Bitm\SEIP1020\Message\Message;

class Gender{


    public $id= "";
    public $gender= "";
    public $conn;


    public function prepare($data = "")
    {
        if (array_key_exists("gender", $data)) {
            $this->gender = $data['gender'];
        }

        if (array_key_exists("id", $data)) {
            $this->id = $data['id'];
        }

    }


    public function __construct()
    {
        $this->conn=mysqli_connect("localhost","root","","atomicprojectb22");
    }

    public function store()
    {
        $query = "INSERT INTO `atomicprojectb22`.`gender` (`gender`) VALUES ('" . $this->gender . "')";
        //echo $query;

        $result = mysqli_query($this->conn, $query);
        if ($result) {
            Message::message("
                <div class=\"alert alert-success\">
                            <strong>Success!</strong> Data has been stored successfully.
                </div>");
            Utility::redirect("index.php");
        } else {
            echo "Error";
        }
    }

    public function index()
    {
        $_allgender = array();
        $query = "SELECT * FROM `gender` WHERE `deleted_at` IS NULL";
        $result = mysqli_query($this->conn, $query);
        while ($row = mysqli_fetch_object($result)) {
            $_allgender[] = $row;
        }

        return $_allgender;
    }

    public function view()
    {
        $query = "SELECT * FROM `gender` WHERE `id`=" . $this->id;
        $result = mysqli_query($this->conn, $query);
        $row = mysqli_fetch_object($result);
        return $row;
    }

    public function edit(){
        $array =array();
        $query = "SELECT * FROM `gender` WHERE `id`=" . $this->id;
        $result = mysqli_query($this->conn,$query);
        if($row = mysqli_fetch_assoc($result)){
            $array[]= explode(",", $row['gender']);
            //echo $row['gender'];
           // var_dump($array[0]);
            return $array[0];
        }
        else
            echo "ERROR!";
    }

    public function update()
    {
        echo $this->gender.$this->id;
        $query = "UPDATE `atomicprojectb22`.`gender` SET `gender` = '".$this->gender."' WHERE `gender`.`id` = ".$this->id;
        //echo $query;

        $result = mysqli_query($this->conn, $query);
        if ($result) {
            Message::message("
                <div class=\"alert alert-success\">
                            <strong>Success!</strong> Data has been updated successfully.
                </div>");
            Utility::redirect("index.php");
        } else {
            echo "Error";
        }
    }

    public function delete(){
        $query = "DELETE FROM `atomicprojectb22`.`gender` WHERE `gender`.`id` = ".$this->id;
        $result = mysqli_query($this->conn,$query);
        if($result){
            Message::message("
<div class=\"alert alert-info\">
  <strong>Deleted!</strong> Data has been deleted successfully.
</div>");
            Utility::redirect("index.php");
        }
        else
        {
            echo "ERROR!";
        }
    }
	
	public function trash()
    {
        $this->deleted_at = time();
        $query = "UPDATE `atomicprojectb22`.`gender` SET `deleted_at` =" . $this->deleted_at . " WHERE `gender`.`id` = " . $this->id;
        $result = mysqli_query($this->conn, $query);
        if ($result) {
            Message::message("
<div class=\"alert alert-info\">
  <strong>Deleted!</strong> Data has been trashed successfully.
</div>");
            Utility::redirect("index.php");
        } else {
            Message::message("
<div class=\"alert alert-info\">
  <strong>Deleted!</strong> Data has not been trashed successfully.
</div>");
            Utility::redirect("index.php");
        }

    }

    public function trashed()
    {
        $_allGender = array();
        $query = "SELECT * FROM `gender` WHERE `deleted_at` IS NOT NULL";
        $result = mysqli_query($this->conn, $query);
        while ($row = mysqli_fetch_object($result)) {
            $_allGender[] = $row;
        }

        return $_allGender;


    }

    public function recover()
    {

        $query = "UPDATE `atomicprojectb22`.`gender` SET `deleted_at` = NULL WHERE `gender`.`id` = " . $this->id;
        $result = mysqli_query($this->conn, $query);
        if ($result) {
            Message::message("
<div class=\"alert alert-info\">
  <strong>Deleted!</strong> Data has been recovered successfully.
</div>");
            Utility::redirect("index.php");
        } else {
            Message::message("
<div class=\"alert alert-info\">
  <strong>Deleted!</strong> Data has not been recovered successfully.
</div>");
            Utility::redirect("index.php");
        }

    }

    public function recoverSeleted($IDs = Array())
    {
        if ((is_array($IDs)) && (count($IDs > 0))) {
            $ids = implode(",", $IDs);
            $query = "UPDATE `atomicprojectb22`.`gender` SET `deleted_at` = NULL WHERE `gender`.`id` IN(" . $ids . ")";
            //echo $query;
            //die();
            $result = mysqli_query($this->conn, $query);
            if ($result) {
                Message::message("
<div class=\"alert alert-info\">
  <strong>Deleted!</strong> Selected Data has been recovered successfully.
</div>");
                Utility::redirect("index.php");
            } else {
                Message::message("
<div class=\"alert alert-info\">
  <strong>Deleted!</strong> Selected Data has not been recovered successfully.
</div>");
                Utility::redirect("index.php");
            }

        }

    }

    public function deleteMultiple($IDs = Array())
    {
        if ((is_array($IDs)) && (count($IDs > 0))) {
            $ids = implode(",", $IDs);
            $query = "DELETE FROM `atomicprojectb22`.`gender` WHERE `gender`.`id`  IN(" . $ids . ")";
            $result = mysqli_query($this->conn, $query);
            if ($result) {
                Message::message("
<div class=\"alert alert-info\">
  <strong>Deleted!</strong> Selected Data has been deleted successfully.
</div>");
                Utility::redirect("index.php");
            } else {
                Message::message("
<div class=\"alert alert-info\">
  <strong>Deleted!</strong> Selected Data has not been deleted successfully.
</div>");
                Utility::redirect("index.php");
            }

        }
    }
	
	   public function count(){
        $query="SELECT COUNT(*) AS totalItem FROM `atomicprojectb22`.`gender` ";
        $result=mysqli_query($this->conn,$query);
        $row= mysqli_fetch_assoc($result);
        return $row['totalItem'];
    }
    public function paginator($pageStartFrom=0,$Limit=5){
        $_allgender = array();
        $query="SELECT * FROM `gender` LIMIT ".$pageStartFrom.",".$Limit;
        $result = mysqli_query($this->conn, $query);
        while ($row = mysqli_fetch_object($result)) {
            $_allgender[] = $row;
        }

        return $_allgender;

    }


}
