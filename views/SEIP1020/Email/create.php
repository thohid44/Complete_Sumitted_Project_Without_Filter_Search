<!DOCTYPE html>
<html lang="en">
<head>
    <title>CRUD-Email</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Create Email Address</h2>
    <form role="form" method="post" action="store.php">
        <div class="form-group">
            <label>Enter Email Address:</label>
            <input type="text" name="email" class="form-control" id="email" placeholder="Enter Email Address">
        </div>

        <button type="submit" class="btn btn-default">Submit</button>
    </form>
</div>

</body>
</html>
