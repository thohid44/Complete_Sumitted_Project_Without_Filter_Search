<?php
//var_dump($_GET);
include_once ('../../../vendor/autoload.php');
use App\Bitm\SEIP1020\Organization\Organization;
use App\Bitm\SEIP1020\Utility\Utility;

$org= new Organization();
$org->prepare($_GET);
$singleItem=$org->view();
//Utility::d($singleItem);
?>





<!DOCTYPE html>
<html lang="en">
<head>
    <title>CRUD-Organization</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Edit Organization </h2>
    <form role="form" method="post" action="update.php">
        <div class="form-group">
            <label>Edit Organization title:</label>
            <input type="hidden" name="id" id="title" value="<?php echo $singleItem->id?>">
         <input type="text" name="title" class="form-control" id="title" value="<?php echo $singleItem->title?>">
            <input type="text" name="des" class="form-control" id="des" placeholder="Enter Organization description" value="<?php echo $singleItem->des?>">
 
 </div>

        <button type="submit" class="btn btn-default">Update</button>
    </form>
</div>

</body>
</html>
