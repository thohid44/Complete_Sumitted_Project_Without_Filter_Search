<?php
//var_dump($_GET);
include_once ('../../../vendor/autoload.php');
use App\Bitm\SEIP1020\City\City;
use App\Bitm\SEIP1020\City\Utility;

$city= new City();
$city->prepare($_GET);
$singleItem=$city->view();
//Utility::d($singleItem);
?>





<!DOCTYPE html>
<html lang="en">
<head>
    <title>CRUD-City</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Edit City Title</h2>
    <form role="form" method="post" action="update.php">
        <div class="form-group">
            <label>Edit City Title:</label>
            <input type="hidden" name="id" id="cityname" value="<?php echo $singleItem->id?>">
            <input type="text" name="cityname" 
			class="form-control" id="cityname" 
			placeholder="Enter Cityname Title" value="<?php echo $singleItem->cityname?>">
        </div>

        <button type="submit" class="btn btn-default">Update</button>
    </form>
</div>

</body>
</html>
