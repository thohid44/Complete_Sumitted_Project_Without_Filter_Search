<!DOCTYPE html>
<html lang="en">
<head>
    <title>CRUD-Birth DAY</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Create Birth Day </h2>
    <form role="form" method="post" action="store.php">
        <div class="form-group">
            <label>Enter Birth Day:</label>
            <input type="text" name="birthdate" class="form-control" id="birthdate" placeholder="Enter Birth Day">
        </div>

        <button type="submit" class="btn btn-default">Submit</button>
    </form>
</div>

</body>
</html>
