<?php
session_start();
include_once('../../../vendor/autoload.php');

use App\Bitm\SEIP1020\ProfilePicture\ImageUploader;
use App\Bitm\SEIP1020\Utility\Utility;
use App\Bitm\SEIP1020\Message\Message;

$profile_picture= new ImageUploader();
$allinfo=$profile_picture->index();
//Utility::d($allBook);

  if(array_key_exists('itemPerPage',$_SESSION)) {
        if (array_key_exists('itemPerPage', $_GET)) {
            $_SESSION['itemPerPage'] = $_GET['itemPerPage'];
        }
    }
    else{
        $_SESSION['itemPerPage']=5;
    }

    //Utility::dd($_SESSION['itemPerPage']);

    $itemPerPage=$_SESSION['itemPerPage'];
    $totalItem=$profile_picture->count();



    $totalPage=ceil($totalItem/$itemPerPage);
    //Utility::dd($itemPerPage);
    $pagination="";


    if(array_key_exists('pageNumber',$_GET)){
        $pageNumber=$_GET['pageNumber'];
    }else{
        $pageNumber=1;
    }
    for($i=1;$i<=$totalPage;$i++){
    $class=($pageNumber==$i)?"active":"";
    $pagination.="<li class='$class'><a href='index.php?pageNumber=$i'>$i</a></li>";
    }

    $pageStartFrom=$itemPerPage*($pageNumber-1);
    $info=$profile_picture->paginator($pageStartFrom,$itemPerPage);



?>

<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

</head>
<body>

<div class="container">
    <h2>All Info List</h2>
    <a href="create.php" class="btn btn-primary" role="button">Create again</a>
	<a href="trashed.php" class="btn btn-primary" role="button">View Trashed list</a>
	<a href="pdf.php" class="btn btn-primary" role="button">Download as PDF</a>
    <a href="xl.php" class="btn btn-primary" role="button">Download as XL</a>
    <a href="mail.php" class="btn btn-primary" role="button">Email to friend</a>

    <div id="message">
        <?php if((array_key_exists('message',$_SESSION)&& (!empty($_SESSION['message'])))) {
            echo Message::message();
        }
        ?>
    </div>
	        <form role="form">
            <div class="form-group">
                <label for="sel1">Select homw many items you want to show (select one):</label>
                <select class="form-control" id="sel1" name="itemPerPage">
                    <option>5</option>
                    <option>10</option>
                    <option selected>15</option>
                    <option>20</option>
                    <option>25</option>
                </select>
                <button type="submit">Go!</button>

            </div>
        </form>
    <div class="table-responsive">
        <table class="table">
            <thead>
            <tr>
                <th>#</th>
                <th>ID</th>
                <th>Name</th>
                <th>Image</th>
                <th>Action</th>

            </tr>
            </thead>
            <tbody>
            <tr>
                <?php
                $sl=0;
                foreach($allinfo as $info){
                $sl++; ?>
                <td><?php echo $sl?></td>
                <td><?php echo $info-> id?></td>
                <td><?php echo $info->name?></td>
                <td><img src="../../../Resources/Images/<?php echo $info->images ?>" alt="image" height="100px" width="100px" class="img-responsive"> </td>
                <td><a href="view.php?id=<?php echo $info-> id ?>" class="btn btn-primary" role="button">View</a>
                    <a href="edit.php?id=<?php echo $info-> id ?>"  class="btn btn-info" role="button">Edit</a>
                    <a href="delete.php?id=<?php echo $info->id?>" class="btn btn-danger" role="button" id="delete"  Onclick="return ConfirmDelete()">Delete</a>
                        <a href="trash.php?id=<?php echo $info->id ?>"  class="btn btn-info" role="button">Trash</a>

                </td>

            </tr>
            <?php }?>

			
			
            </tbody>
        </table>
		
		
    </div>
	
	<div>      <ul class="pagination">

            <?php

            if($pageNumber>1  )

            {
                echo ". <li><a href=\"#\">Prev</a></li>."  ;


            }
            echo $pagination;
            if($pageNumber<$totalPage) {
                echo ". <li><a href=\"#\">Next</a></li>.";

            }

            ?>

        </ul>
		</div>
</div>
<script>
    $('#message').show().delay(2000).fadeOut();


    //        $(document).ready(function(){
    //            $("#delete").click(function(){
    //                if (!confirm("Do you want to delete")){
    //                    return false;
    //                }
    //            });
    //        });
    function ConfirmDelete()
    {
        var x = confirm("Are you sure you want to delete?");
        if (x)
            return true;
        else
            return false;
    }

</script>

</body>
</html>
